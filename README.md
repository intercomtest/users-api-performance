# README #

JMeter performance scripts for the users-api stack.

### How to execute ###

Download jMeter and clone this repo.
Execute the cmd: 'jmeter -n -t users-api.jmx' via the command line.
The jmeter executable is within the bin folder of the JMeter download.
